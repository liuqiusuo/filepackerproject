﻿using System;
using System.IO;
using System.Collections.Generic;
using PackFile;
using System.Threading;
using System.Linq;

namespace FileReadTest
{
	public static class MainClass
	{
		
		public static void Main (string[] args)
		{
			TestPerformance();

		}
		static void GetFilePath_few(out List<string> filePathList, out List<string> pckFilesList)
		{
			List<string> DirPathList = new List<string> () 
			{
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Sound",
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Pic"
			};

			filePathList = new List<string> ();
			foreach (var path in DirPathList) 
			{
				filePathList.AddRange(GetTestFileFullPathList (path));
			}

			pckFilesList = new List<string>()
			{
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Sound/Sound.pck",
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Pic/Pic.pck"
			};
		}

		static void GetFilePath_much(out List<string> filePathList, out List<string> pckFilesList)
		{
			List<string> DirPathList = new List<string> () 
			{
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Resources"
			};

			filePathList = new List<string> ();
			foreach (var path in DirPathList) 
			{
				filePathList.AddRange(GetTestFileFullPathList (path));
			}

			pckFilesList = new List<string>()
			{
				"/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Resources/file.pck"
			};			
		}

		static void TestPerformance()
		{
			
			Console.WriteLine ("\n#Performace Test Start#\n");

			//1.少量文件
			List<string> filePathList_few;
			List<string> pckFilesList_few;
			GetFilePath_few(out filePathList_few, out pckFilesList_few);

			//循环多次取平均值
			int loopTimes_few = 10;
			LoopTestAndOutput (filePathList_few, pckFilesList_few, loopTimes_few);


			//2.大量文件
			List<string> filePathList_much;
			List<string> pckFilesList_much;
			GetFilePath_much(out filePathList_much, out pckFilesList_much);

			//循环多次取平均值
			int loopTimes_much = 10;
			LoopTestAndOutput (filePathList_much, pckFilesList_much, loopTimes_much);

			Console.WriteLine ("\n#Performace Test Over#\n");
		}

		static void LoopTestAndOutput(List<string> filePathList, List<string> pckFilesList, int loopTimes)
		{
			string noDiskBuffer = "-------------NO DISK BUFFER-------------";
			string withDiskBuffer = "-------------WITH DISK BUFFER-------------";

			Console.WriteLine ("\nFile Num: {0}", filePathList.Count);
			Console.WriteLine ("Average Size: {0}", GetAveSize(filePathList));

			Console.WriteLine ("\n" + noDiskBuffer);
			CalFileReadTimeSpend (filePathList, pckFilesList, loopTimes, false);//true

			Console.WriteLine ("\n" + withDiskBuffer);
			CalFileReadTimeSpend (filePathList, pckFilesList, loopTimes, false);
		}


		static long GetAveSize(List<string> pathList)
		{
			long totalSize = 0;
			foreach (string path in pathList) 
			{
				if (File.Exists (path)) 
				{
					totalSize += new FileInfo (path).Length;
				}
			}

			return totalSize / pathList.Count;
		}

		static void GetAllTimeSpend (List<string> filePathList, List<string> pckFilesList, int LoopTimes, bool DoPurge, 
			out List<float> SysExistsTimeSpendList,
			out List<float> SysReadTimeSpendList, 
			out List<float> NewAPILoadTimeSpendList,
			out List<float> NewAPIExistsTimeSpendList,
			out List<float> NewAPIReadTimeSpendList
		)
		{
			SysReadTimeSpendList = new List<float> ();
			SysExistsTimeSpendList = new List<float> ();

			NewAPIReadTimeSpendList = new List<float> ();
			NewAPIExistsTimeSpendList = new List<float> ();
			NewAPILoadTimeSpendList = new List<float> ();

			for (int i = 0; i < LoopTimes; i++) 
			{
				Console.Write (".");
				if (DoPurge) 
				{
					PurgeDiskBuffer ();
				}

				float sysExistsTimeSpend = TestSysExistsTimeSpendByPathList(filePathList);
				float sysReadTimeSpend = TestSysReadTimeSpendByPathList(filePathList);

				float newApiLoadTimeSpend = TestNewApiLoad(pckFilesList);
				float newApiExistsTimeSpend = TestNewApiExistsTimeSpend (filePathList);
				float newApiReadTimeSpend = TestNewApiReadTimeSpend(filePathList);

				SysExistsTimeSpendList.Add (sysExistsTimeSpend);
				SysReadTimeSpendList.Add (sysReadTimeSpend);


				NewAPILoadTimeSpendList.Add (newApiLoadTimeSpend);
				NewAPIExistsTimeSpendList.Add(newApiExistsTimeSpend);
				NewAPIReadTimeSpendList.Add(newApiReadTimeSpend);
			}			
		}

		static void OutPutInfo (List<float> SysExistsTimeSpendList, List<float> SysReadTimeSpendList,
			List<float> NewAPILoadTimeSpendList, List<float> NewAPIExistsTimeSpendList, List<float> NewAPIReadTimeSpendList,
			int fileCount=1
		)
		{
			Console.WriteLine ();
			int roundN = 4;
			//int roundPerN = 8;

			for (int i = 0; i < SysExistsTimeSpendList.Count; i++) 
			{
				Console.WriteLine ("SysExst:{0},\tSysRead:{1};\tNewExst:{2},\tNewRead:{3},\t{4}(Load)", 
					SysExistsTimeSpendList[i].RoundN(roundN), SysReadTimeSpendList[i].RoundN(roundN),
					NewAPIExistsTimeSpendList[i].RoundN(roundN), NewAPIReadTimeSpendList[i].RoundN(roundN), NewAPILoadTimeSpendList[i].RoundN(roundN)
				);
			}

			float AveSysExistsTimeSpend = Average(SysExistsTimeSpendList);
			float AveSysReadTimeSpend = Average(SysReadTimeSpendList);

			float AveNewAPILoadTimeSpend = Average(NewAPILoadTimeSpendList);
			float AveNewAPIExistsTimeSpend = Average(NewAPIExistsTimeSpendList);
			float AveNewAPIReadTimeSpend = Average(NewAPIReadTimeSpendList);

			Console.WriteLine ("------------------------------AVERAGE (s)-----------------------------");

			Console.WriteLine ("SysExst:{0},\tSysRead:{1};\tNewExst:{2},\tNewRead:{3},\t{4}(Load)", 
				AveSysExistsTimeSpend.RoundN(roundN), AveSysReadTimeSpend.RoundN(roundN), 
				AveNewAPIExistsTimeSpend.RoundN(roundN), AveNewAPIReadTimeSpend.RoundN(roundN), AveNewAPILoadTimeSpend.RoundN(roundN));

			Console.WriteLine ("--------------------------AVERAGE PER FILE (ms)------------------------");

			Console.WriteLine ("SysExst:{0},\tSysRead:{1};\tNewExst:{2},\tNewRead:{3}", 
				(AveSysExistsTimeSpend/fileCount * 1000).RoundN(roundN), 
				(AveSysReadTimeSpend/fileCount * 1000).RoundN(roundN), 
				(AveNewAPIExistsTimeSpend/fileCount * 1000).RoundN(roundN), 
				(AveNewAPIReadTimeSpend/fileCount * 1000).RoundN(roundN)
				//,(AveNewAPILoadTimeSpend/fileCount * 1000).RoundN(roundN)
			);


		}

		public static float RoundN(this float num, int roundN)
		{
			return (float)Math.Round (num, roundN);
		}

		static float Average(IEnumerable<float> numList)
		{
			float sum = 0;
			foreach (float num in numList) 
			{
				sum += num;
			}
			return sum / numList.Count();
		}

		static void CalFileReadTimeSpend(List<string> filePathList, List<string> pckFilesList, int LoopTimes, bool doPurge)
		{
			List<float> SysExistsTimeSpendList;
			List<float> SysReadTimeSpendList;

			List<float> NewAPILoadTimeSpendList;
			List<float> NewAPIExistsTimeSpendList;
			List<float> NewAPIReadTimeSpendList;

			GetAllTimeSpend (filePathList, pckFilesList, LoopTimes, doPurge, 
				out SysExistsTimeSpendList, out SysReadTimeSpendList,
				out NewAPILoadTimeSpendList, out NewAPIExistsTimeSpendList, out NewAPIReadTimeSpendList
			);

			OutPutInfo (SysExistsTimeSpendList, SysReadTimeSpendList,
				NewAPILoadTimeSpendList, NewAPIExistsTimeSpendList, NewAPIReadTimeSpendList,
				filePathList.Count
			);
				
		}

		static float CalTime(List<String>filePathList, Action<string> action)
		{
			DateTime begin = DateTime.Now;
			foreach (string path in filePathList) 
			{
				action (path);
			}
			DateTime end = DateTime.Now;

			float timeSpend = (float)(end - begin).TotalSeconds;
			return timeSpend;
		}

		static float TestNewApiExistsTimeSpend(List<string> filePathList)
		{
			Action<string>action = (string path)=>{
				PackedFile.Exists(path);
			};

			return CalTime (filePathList,action);
		}
		static float TestSysExistsTimeSpendByPathList(List<string> filePathList)
		{
			Action<string>action = (string path)=>{
				File.Exists(path);
			};

			return CalTime (filePathList,action);

		}
		static float TestSysReadTimeSpendByPathList(List<string> filePathList)
		{
			Action<string>action = (string path)=>{
				byte[] Bytes;
				Bytes = File.ReadAllBytes (path);

			};

			return CalTime (filePathList,action);
		}


		static float TestNewApiLoad(List<string> pckFilesList)
		{
			Action<string>action = (string path)=>{
				if (File.Exists (path))
				{
					PackedFile.Load (path);
				}
			};

			return CalTime (pckFilesList,action);
		}

		static float TestNewApiReadTimeSpend(List<string> filePathList)
		{
			Action<string>action = (string path)=>{
				byte[] Bytes;
				Bytes = PackedFile.ReadAllBytes (path);
			};

			return CalTime (filePathList,action);
		}

		public static void PurgeDiskBuffer()
		{
			System.Diagnostics.Process p = new System.Diagnostics.Process();
			p.StartInfo.FileName = "sh";
			p.StartInfo.UseShellExecute = false;
			p.EnableRaisingEvents = true;
			p.StartInfo.RedirectStandardInput = true;
			p.StartInfo.RedirectStandardOutput = false;
			p.StartInfo.CreateNoWindow = true;	

			p.Start();
			p.StandardInput.WriteLine ("echo \"dispass\" | sudo -S purge");
			p.StandardInput.WriteLine ("exit");
			p.WaitForExit ();
			p.Close();

			Thread.Sleep (10000);
		}


		public static List<string> GetTestFileFullPathList(string mainPath)
		{
			List<DirectoryInfo> DirInfoList = new List<DirectoryInfo>();

			DirInfoList.Add(new DirectoryInfo(mainPath));
			int DirInfoListIndex = 0;


			while (DirInfoListIndex < DirInfoList.Count)
			{
				DirectoryInfo[] dirs = new DirectoryInfo[] { };
				try
				{
					dirs = DirInfoList[DirInfoListIndex].GetDirectories();

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				DirInfoList.AddRange(dirs);
				DirInfoListIndex++;
			}


			List<String> FilePathList = new List<string>();

			foreach (var dirInfo in DirInfoList)
			{
				FileInfo[] tFileInfos = new FileInfo[] { };

				try
				{
					tFileInfos = dirInfo.GetFiles();

				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}


				foreach (var fileInfo in tFileInfos)
				{
					string subFilePath = fileInfo.FullName;

					if (!subFilePath.ToLower ().EndsWith (".pck")) 
					{
						FilePathList.Add (subFilePath);
					}

				}
			}

//			Random rnd = new Random ();
//			return FilePathList.OrderBy(key=>rnd.Next()).ToList();
			return FilePathList;
		}
	}
}
