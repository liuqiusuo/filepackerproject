﻿using NUnit.Framework;
using System;
using System.IO;
using System.Collections.Generic;
using PackFile;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace UnitTest
{
	[TestFixture (), RequiresMTA]
	public class TestPackFileAPI
	{
		List<string> FilePathList = new List<string>();
		string path1 = "/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Sound";
		string path2 = "/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/Pic";
		string pathLoad = "/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/TestLoad";

		class Int
		{
			public int value;
			public Int(int num)
			{
				this.value = num;
			}
		}

		[SetUp]
		public void GetFileList()
		{
			FilePathList.AddRange(GetTestFileFullPathList( path1 ));
			FilePathList.AddRange(GetTestFileFullPathList( path2 ));

			LoadTestFiles ();
		}


		void LoadTestFiles()
		{
			PackedFile.Load(path1 + "/Sound.pck");
			PackedFile.Load(path2 + "/Pic.pck");
		}


		[Test]
		public void TestLoad()
		{
			Assert.False(PackedFile.Exists (pathLoad + "/change.txt"));
			Assert.False(PackedFile.Exists (pathLoad + "/new.txt"));
			Assert.False(PackedFile.Exists (pathLoad + "/old.txt"));

			PackedFile.Load(pathLoad + "/oldPack.pck");

			Assert.True(PackedFile.Exists (pathLoad + "/change.txt"));
			Assert.False(PackedFile.Exists (pathLoad + "/new.txt"));
			Assert.True(PackedFile.Exists (pathLoad + "/old.txt"));

			string unchanged = GetStringFrom (pathLoad + "/change.txt");
			Assert.True (unchanged == "unchanged");

			string old = GetStringFrom (pathLoad + "/old.txt");
			Assert.True (old == "old");

			PackedFile.Load(pathLoad + "/newPack.pck");

			string changed = GetStringFrom (pathLoad + "/change.txt");
			Assert.True (changed == "changed");

			string old2 = GetStringFrom (pathLoad + "/old.txt");
			Assert.True (old2 == "old");

			string new0 = GetStringFrom (pathLoad + "/new.txt");
			Assert.True (new0 == "new");
		}

		string GetStringFrom(string path)
		{
			byte[] Bytes = PackedFile.ReadAllBytes (path);
			string text = System.Text.Encoding.UTF8.GetString(Bytes);
			return text;
		}

		[Test]
		public void TestExits ()
		{
			foreach (string path in FilePathList) 
			{
				Assert.False (PackedFile.Exists(path + "111") );
				Assert.True (PackedFile.Exists(path));
			}
		}

		[Test]
		public void TestGetAllBytes ()
		{
			foreach (string path in FilePathList) 
			{
				Assert.True (CompareFileRead_GetAllBytes(path));
			}
		}

		[Test]
		public void TestGetStream ()
		{
			foreach (string path in FilePathList) 
			{
				Assert.True (CompareFileRead_GetStream(path));

			}
		}

		[Test]
		public void TestThreadGetAllBytes()
		{
			Func<bool> func = delegate(){
				string path = GetRandomFilePath();
				return CompareFileRead_GetAllBytes(path);
				//return false;
			};

			MultiThreadTest (func, 100, 300);
		}

		[Test]
		public void TestThreadGetStream()
		{
			Func<bool> func = delegate(){
				string path = GetRandomFilePath();
				return CompareFileRead_GetStream(path);
			};

			MultiThreadTest (func, 100, 300);				
		}

		[Test]
		public void TestThreadAll()
		{

			Func<bool> func = delegate(){
				LoadTestFiles();
				return true;
			};

			MultiThreadTest (func, 10, 100);

			TestThreadGetStream ();
			TestThreadGetAllBytes ();
		}
	
		void MultiThreadTest(Func<bool> func, int threadNum, int loopTimes)
		{
			bool commFlag = true;
			Int overCount = new Int(0);

			ThreadStart ts = new ThreadStart (
				delegate{
					int times = loopTimes;
					bool flag = true;

					for(int i = 0;i< times;i++)
					{
						if(func() == false)
						{
							flag = false;
							break;
						}

					}
					if(flag == false)
					{
						commFlag = flag;
					}
					Interlocked.Increment(ref overCount.value);
				}
			);

			for (int i = 0; i < threadNum; i++) 
			{
				Thread workThread = new Thread(ts);
				workThread.Name = "Thread_GetAllBytes_" + i;
				workThread.Start ();
			}

			while (true) 
			{
				Thread.Sleep (100);
				Assert.True (commFlag);
				if (overCount.value >= threadNum) 
				{
					break;
				}
			}
			Assert.True (commFlag);
		}


		string GetRandomFilePath()
		{
			Random r = new Random ();
			int index = r.Next (0, FilePathList.Count);
			return FilePathList[index];
		}

		bool CompareFileRead_GetAllBytes(string path)
		{
			byte[] originalData = File.ReadAllBytes(path);
			byte[] packedData = PackedFile.ReadAllBytes(path);

			return CompareTwoByteArray (originalData, packedData);
		}

		bool CompareFileRead_GetStream(string path)
		{
			byte[] originalData = File.ReadAllBytes(path);
			PackedFileStreamReader pfsr = PackedFile.GetStreamReader (path);
			byte[] packedData = new byte[pfsr.Length];

			int len1 = new Random ().Next (0, (int)pfsr.Length);
			int len2 = (int)pfsr.Length - len1;

			pfsr.Read (packedData, 0, len1);
			pfsr.Read (packedData, len1, len2);

			return CompareTwoByteArray (originalData, packedData);
		}

		bool CompareTwoByteArray(byte[] originalData, byte[] packedData)
		{
			bool flag = true;

			for (int i = 0; i < originalData.Length; i++) 
			{
				if (originalData [i] != packedData [i]) 
				{
					flag = false;
					break;
				}
			}

			return flag;
		}



		public static List<string> GetTestFileFullPathList(string mainPath)
		{
			List<DirectoryInfo> DirInfoList = new List<DirectoryInfo>();

			DirInfoList.Add(new DirectoryInfo(mainPath));
			int DirInfoListIndex = 0;


			while (DirInfoListIndex < DirInfoList.Count)
			{
				DirectoryInfo[] dirs = new DirectoryInfo[] { };
				try
				{
					dirs = DirInfoList[DirInfoListIndex].GetDirectories();

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				DirInfoList.AddRange(dirs);
				DirInfoListIndex++;
			}


			List<String> FilePathList = new List<string>();

			foreach (var dirInfo in DirInfoList)
			{
				FileInfo[] tFileInfos = new FileInfo[] { };

				try
				{
					tFileInfos = dirInfo.GetFiles();

				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}


				foreach (var fileInfo in tFileInfos)
				{
					string subFilePath = fileInfo.FullName;
	
					if (!subFilePath.ToLower ().EndsWith (".pck")) 
					{
						FilePathList.Add (subFilePath);
					}
				}
			}

			return FilePathList;
		}
	}
}
