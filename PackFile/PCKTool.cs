﻿using System;
using System.Net;
using System.IO;

namespace PackFile
{
	class PCKTool
	{
		bool BigEndian = false;
		char SystemSlash = '/';

		public PCKTool()
		{
			CheckSystemSlashType ();
			CheckAndSetEndianType ();
		}

		public void CheckSystemSlashType()
		{
			string path = System.Environment.CurrentDirectory;
			if(path.IndexOf('\\') >= 0)
			{
				//windows系统
				SystemSlash = '\\';
			}
			else 
			{
				SystemSlash = '/';
			}
		}

		public void CheckAndSetEndianType()
		{
			long num = 1;
			byte[] bytes = BitConverter.GetBytes (num);
			if (bytes [0] == 0) 
			{
				BigEndian = true;
			}
		}

		public long GetStringBytesLength(string str)
		{
			byte[] StrBytes = StringToBytes (str);
			return StrBytes.Length;
		}

		public long BytesToLong(byte[] bytes)
		{
			long num = BitConverter.ToInt64(bytes,0);

			if (BigEndian) 
			{
				num = IPAddress.NetworkToHostOrder(num); //big to small
			}

			return num;
		}

		public string AdjustSystemSlash(string path)
		{
			if (SystemSlash == '\\') 
			{
				//windows 
				path = path.Replace('/', '\\');
			}
			return path;
		}

		public string UnifyPath(string path)
		{
			return Path.GetFullPath (path).ToLower();
		}

		public string BytesToString(byte[] Bytes)
		{
			return System.Text.Encoding.UTF8.GetString(Bytes);
		}

		public byte[] StringToBytes(string str)
		{
			//统一使用 UTF8 方式
			return System.Text.Encoding.UTF8.GetBytes(str);			
		}

		public byte[] LongToBytes(long num)
		{
			//大小端
			if(BigEndian)
			{
				num = IPAddress.HostToNetworkOrder(num); //small to big
			}
			//定长 8byte
			byte[] bytes = BitConverter.GetBytes(num);

			return bytes;
		}

		public string DealSystemFileSlashDiff(string filePath)
		{
			//统一使用"/"方式 
			return filePath.Replace ('\\','/');
		}

		public string GetRelativePathWithoutDirPath(FileInfo fi, string rsc)
		{
			string fullPath = Path.GetFullPath (rsc);
			string fileName = fi.FullName;
			if (fileName.StartsWith (fullPath)) 
			{
				fileName = fileName.Substring (fullPath.Length);
			}
			return fileName;
		}
		public string GetFullFilePathByPckPath(string pckPath, string resourcePath)
		{
			int index = pckPath.LastIndexOf(SystemSlash);
			if (index >= 0) 
			{
				resourcePath = pckPath.Substring(0, index) + resourcePath;
			}

			return resourcePath;
		}

		public long UpTo8Times(long num)
		{
			return ((long)Math.Ceiling(num / 8.0)) * 8;
		}

		public int UpTo8Times(int num)
		{
			return ((int)Math.Ceiling(num / 8.0)) * 8;
		}
	}
}

