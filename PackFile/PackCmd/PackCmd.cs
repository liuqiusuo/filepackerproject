﻿using System;
using System.IO;
using System.Collections.Generic;
using PackFile;

namespace PackFile
{
	public class PackCmd
	{
		static PCKTool pckTool;
		public static void Pack(string des, string rsc)
		{
			pckTool = new PCKTool ();

			if (!des.ToLower().EndsWith ("." + PCK_Protocol.postfix)) 
			{
				des = des + "." + PCK_Protocol.postfix;
			}

			string realDes = des;
			des = des +".tmp";

			try{

				//获取文件列表信息
				List<FileInfo> FileInfoList = GetFileInfoList (rsc);

				//构建文件信息列表
				Dictionary<string, PackedFileInfo> PackedFilesInfoDic = new Dictionary<string, PackedFileInfo>();
				long offset = 0;
				long byteSize = 0;

				//构建每个文件信息
				foreach(var fileInfo in FileInfoList)
				{
					string filePath = pckTool.GetRelativePathWithoutDirPath(fileInfo, rsc);
					filePath = pckTool.DealSystemFileSlashDiff(filePath);
					filePath = filePath.ToLower();
						
					PackedFileInfo pfi = new PackedFileInfo();

					pfi.PathStringLength = pckTool.GetStringBytesLength(filePath);
					pfi.PathString = filePath;
					pfi.FileDataOffset = offset;
					pfi.FileDataSize = fileInfo.Length;

					if(PackedFilesInfoDic.ContainsKey(filePath))
					{
						throw(new Exception("There are two same pathes!"));
					}
					else 
					{
						PackedFilesInfoDic.Add(filePath, pfi);
					}


					offset += pckTool.UpTo8Times(fileInfo.Length);

					byteSize += PCK_Protocol.HEAD_PATH_STRING_LENGTH_SIZE + pckTool.UpTo8Times(pfi.PathStringLength) 
						+ PCK_Protocol.HEAD_FILE_OFFSET_SIZE + PCK_Protocol.HEAD_FILE_SIZE_SIZE;
				}

				//重新设置文件数据offset
				foreach(var elem in PackedFilesInfoDic)
				{
					elem.Value.FileDataOffset += byteSize + PCK_Protocol.FILE_HEAD_TOTAL_SIZE;
				}


				//声明待写入Stream
				Stream CommHeadStream;
				Stream FileInfosStream;
				Stream FileDataStream;

				//写公共头stream
				CommHeadStream = GetCommHeadStream(byteSize);

				//写文件信息stream
				FileInfosStream = GetFileInfosStream(PackedFilesInfoDic);

				//写文件数据stream
				FileDataStream = GetFileDataStream(FileInfoList);

				//整合所有stream写到文件
				if(File.Exists(des))
				{
					File.Delete(des);
				}
				using (FileStream fout = new FileStream(des, FileMode.Create, FileAccess.Write))
				{
					CommHeadStream.CopyTo(fout);
					FileInfosStream.CopyTo(fout);
					FileDataStream.CopyTo(fout);
				}
			}
			catch(Exception e) 
			{
				Console.WriteLine (e.Message);
				if (File.Exists (des)) 
				{
					File.Delete (des);
				}
			}

			if (File.Exists (realDes)) 
			{
				File.Delete (realDes);
			}
			if (File.Exists (des)) 
			{
				File.Move (des, realDes);
			}

		}

		static Stream GetCommHeadStream(long headInfoLength)
		{
			byte[] FileTypeSignBytes = pckTool.StringToBytes(PCK_Protocol.FileTypeSign);
			byte[] VersionBytes = pckTool.StringToBytes(PCK_Protocol.Version);
			byte[] HeadInfoLengthBytes = pckTool.LongToBytes (headInfoLength);

			MemoryStream ms = new MemoryStream ();
			ms.WriteWithBlankBytes (FileTypeSignBytes, 0, PCK_Protocol.FILE_TYPE_SIGN_SIZE);
			ms.WriteWithBlankBytes (VersionBytes, 0, PCK_Protocol.FILE_VERSION_SIZE);
			ms.WriteWithBlankBytes (HeadInfoLengthBytes, 0, PCK_Protocol.FILE_FILELIST_HEAD_LENGTH_SIZE);

			ms.Position = 0;
			return ms;
		}

		/**
			 * Head Data Format
			 * 
			 * --Head Data 1
			 * ----MD5 of file path, 32 Bytes
			 * ----file data offset, 8 Bytes
			 * ----file data size, 8 Bytes
			 * 
			 * --Head Data 2
			 * ----...
			 * 
			 * --Head Data 3
			 * ----... 
			 * 
			 */

		static Stream GetFileInfosStream(Dictionary<string, PackedFileInfo> fileInfoDic)
		{
			MemoryStream ms = new MemoryStream ();
			foreach (var elem in fileInfoDic) 
			{
				PackedFileInfo packedFileInfo = elem.Value;

				byte[] PathLengthBytes = pckTool.LongToBytes(packedFileInfo.PathStringLength);
				byte[] PathStringBytes = pckTool.StringToBytes(packedFileInfo.PathString);
				byte[] FileOffsetBytes = pckTool.LongToBytes(packedFileInfo.FileDataOffset);
				byte[] FileSizeBytes = pckTool.LongToBytes(packedFileInfo.FileDataSize);

				ms.WriteWithBlankBytes (PathLengthBytes, 0, PCK_Protocol.HEAD_PATH_STRING_LENGTH_SIZE);
				ms.WriteWithBlankBytes (PathStringBytes, 0, (int)pckTool.UpTo8Times(packedFileInfo.PathStringLength));
				ms.WriteWithBlankBytes (FileOffsetBytes, 0, PCK_Protocol.HEAD_FILE_OFFSET_SIZE);
				ms.WriteWithBlankBytes (FileSizeBytes, 0, PCK_Protocol.HEAD_FILE_SIZE_SIZE);
			}

			ms.Position = 0;
			return ms;
		}

		static Stream GetFileDataStream(List<FileInfo> fileInfoList)
		{
			MemoryStream ms = new MemoryStream ();
			foreach (var fileInfo in fileInfoList) 
			{
				byte[] fileBytes = File.ReadAllBytes (fileInfo.FullName);
				ms.WriteWithBlankBytes(fileBytes, 0, pckTool.UpTo8Times (fileBytes.Length));
			}

			ms.Position = 0;
			return ms;
		}

		static List<FileInfo> GetFileInfoList(string rsc)
		{
			List<DirectoryInfo> DirInfoList = new List<DirectoryInfo>();

			DirInfoList.Add(new DirectoryInfo(rsc));
			int DirInfoListIndex = 0;

			while (DirInfoListIndex < DirInfoList.Count)
			{
				DirectoryInfo[] dirs = new DirectoryInfo[] { };
				try
				{
					dirs = DirInfoList[DirInfoListIndex].GetDirectories();

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				DirInfoList.AddRange(dirs);
				DirInfoListIndex++;
			}

			List<FileInfo> fileInfoList = new List<FileInfo> ();
			foreach (DirectoryInfo dirInfo in DirInfoList) 
			{
				fileInfoList.AddRange(dirInfo.GetFiles ());
			}

			return fileInfoList;
		}
	}

	public static class ExtendMethod
	{
		//长度不足补空byte
		public static void WriteWithBlankBytes(this Stream stream, byte[] buffer, int offset, int realLength)
		{
			int count = buffer.Length - offset;

			if (realLength > count) 
			{
				stream.Write (buffer, offset, count);
				byte[] blankBytes = new byte[realLength - count];

				stream.Write (blankBytes, 0, blankBytes.Length);
			} 
			else 
			{
				stream.Write (buffer, offset, realLength);
			}

		}
	}

}

