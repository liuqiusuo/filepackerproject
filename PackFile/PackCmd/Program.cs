﻿using System;
using System.IO;
using PackFile;

namespace PackFileCmd
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string targetPath;
			string resourcePath;

			if (true) 
			{
				if (args.Length < 2) {
					Console.WriteLine ("need 2 arguments!");
					return;
				}
					
				targetPath = args [0];
				resourcePath = args [1];

				if (!Directory.Exists (resourcePath)) {
					Console.WriteLine ("Pack directory is not existed!");
					return;				
				}
			}

//			string targetDir = "TestLoad";
//
//			targetPath = "/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/" + targetDir + "/" + targetDir + ".pck";
//			resourcePath = "/Users/admin/Projects/PackFile/FileReadTest/bin/Debug/" + targetDir;

			Console.WriteLine ("packing...");

			DateTime start = DateTime.Now;
			PackCmd.Pack (targetPath, resourcePath);
			DateTime stop = DateTime.Now;

			Console.WriteLine ("files packed! time spend {0} second!",stop - start);

		}
	}
}
