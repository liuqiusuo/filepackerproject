using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace PackFile
{
	public class PackedFile
	{
		static PackedFile Instance = new PackedFile ();
		Dictionary<string, UnpackedFileInfo> PackedFileInfoDic = new Dictionary<string, UnpackedFileInfo> ();

		static Mutex commMut = new Mutex ();

		PCKTool pckTool;

		PackedFile ()
		{
			pckTool = new PCKTool ();
		}

		~PackedFile()
		{
			ReleaseAllFiles ();

			Instance = null;
		}
		void ReleaseAllFiles()
		{
			foreach (var elem in PackedFileInfoDic) 
			{
				elem.Value.Fs.Close ();
			}
		}

		void ParseOnePckFile(string path)
		{
			FileStream fs = null;
			try
			{
				path = Path.GetFullPath(path);

				fs = new FileStream (path, FileMode.Open, FileAccess.Read);
				//Mutex mut = new Mutex();

				//读头标记等
				byte[] FileTypeSignBytes = new byte[PCK_Protocol.FILE_TYPE_SIGN_SIZE];
				byte[] VersionBytes = new byte[PCK_Protocol.FILE_VERSION_SIZE];
				byte[] HeadInfoLengthBytes = new byte[PCK_Protocol.FILE_FILELIST_HEAD_LENGTH_SIZE];

				fs.Read(FileTypeSignBytes, 0, PCK_Protocol.FILE_TYPE_SIGN_SIZE);
				fs.Read(VersionBytes, 0, PCK_Protocol.FILE_VERSION_SIZE);
				fs.Read(HeadInfoLengthBytes, 0, PCK_Protocol.FILE_FILELIST_HEAD_LENGTH_SIZE);

				//获取头部文件信息列表长度
				long headInfoLength = pckTool.BytesToLong(HeadInfoLengthBytes);

				//读头部信息列表数据块
				byte[] HeadInfoListBytes = new byte[headInfoLength];
				fs.Read(HeadInfoListBytes, 0, (int)headInfoLength);
				MemoryStream ms = new MemoryStream(HeadInfoListBytes);

				//解析头部信息列表
				while(ms.Position < headInfoLength)
				{
					//解析每个头部信息

					//获取路径长度
					byte[] PathStringLengthBytes = new byte[PCK_Protocol.HEAD_PATH_STRING_LENGTH_SIZE];
					ms.Read(PathStringLengthBytes, 0, PCK_Protocol.HEAD_PATH_STRING_LENGTH_SIZE);
					long pathStringLength = pckTool.BytesToLong(PathStringLengthBytes);

					//获取路径
					byte[] PathStringBytes = new byte[pathStringLength];
					ms.Read(PathStringBytes, 0, (int)pathStringLength);
					ms.Position += pckTool.UpTo8Times(pathStringLength) - pathStringLength;//读字符串8字节对齐空缺
					string pathString = pckTool.BytesToString(PathStringBytes);
					pathString = pckTool.AdjustSystemSlash(pathString);

					//获取数据段偏移和长度
					byte[] FileOffsetBytes = new byte[PCK_Protocol.HEAD_FILE_OFFSET_SIZE];
					byte[] FileSizeBytes = new byte[PCK_Protocol.HEAD_FILE_SIZE_SIZE];
					ms.Read(FileOffsetBytes, 0, PCK_Protocol.HEAD_FILE_OFFSET_SIZE);
					ms.Read(FileSizeBytes, 0, PCK_Protocol.HEAD_FILE_SIZE_SIZE);
					long fileOffset = pckTool.BytesToLong(FileOffsetBytes);
					long fileSize = pckTool.BytesToLong(FileSizeBytes);

					//生成信息结构
					UnpackedFileInfo upfi = new UnpackedFileInfo();
					upfi.Path = pathString;
					upfi.FileDataOffset = fileOffset;
					upfi.FileDataSize = fileSize;
					upfi.Fs = fs;
					//upfi.FileMut = mut;

					//加入列表
					string fullFilePath = pckTool.GetFullFilePathByPckPath(path, pathString);
					fullFilePath = pckTool.UnifyPath(fullFilePath);

					if(PackedFileInfoDic.ContainsKey(fullFilePath))
					{
						PackedFileInfoDic[fullFilePath] = upfi;
					}
					else
					{
						PackedFileInfoDic.Add(fullFilePath, upfi);
					}
				}
			}
			catch(Exception e)
			{
				throw(e);
			}
				
		}

		#region  Public API

		public static void Load(string path)
		{
			commMut.WaitOne ();

			Instance.ParseOnePckFile (path);

			commMut.ReleaseMutex ();
		}

		public static PackedFileStreamReader GetStreamReader (string filePath)
		{
			
			PackedFileStreamReader pfsr = null;

			filePath = Instance.pckTool.UnifyPath (filePath);

			UnpackedFileInfo ufi;
			if (Instance.PackedFileInfoDic.TryGetValue (filePath, out ufi)) 
			{
				pfsr = new PackedFileStreamReader (ufi.Fs, commMut, ufi.FileDataOffset, ufi.FileDataSize);
			}
			else 
			{
				throw(new Exception ("The file \"" + filePath + "\" is not found!"));
			}

			return pfsr;
		}

		public static bool Exists(string filePath)
		{
			string path = Instance.pckTool.UnifyPath (filePath);
			return Instance.PackedFileInfoDic.ContainsKey (path);
		}

		public static byte[] ReadAllBytes(string filePath)
		{
			byte[] Bytes = new byte[0];
			filePath = Instance.pckTool.UnifyPath (filePath);

			UnpackedFileInfo ufi;
			if (Instance.PackedFileInfoDic.TryGetValue (filePath, out ufi)) 
			{
				Stream stream = ufi.Fs;
				//Mutex mut = ufi.FileMut;
				long fileOffset = ufi.FileDataOffset;
				long fileSize = ufi.FileDataSize;

				commMut.WaitOne ();

					stream.Position = fileOffset;
					Bytes = new byte[fileSize];
					stream.Read (Bytes, 0, (int)fileSize);

				commMut.ReleaseMutex();

			} else {
				throw(new Exception ("The file \"" + filePath + "\" is not found!"));
			}

			return Bytes;
		}

		#endregion
	}
}

