﻿using System;
using System.IO;
using System.Threading;

namespace PackFile
{
	class PackedFileInfo
	{
		public long PathStringLength;
		public string PathString;
		public long FileDataOffset;
		public long FileDataSize;
	}
	class UnpackedFileInfo
	{
		public string Path;
		public long FileDataOffset;
		public long FileDataSize;
		public FileStream Fs;
		//public Mutex FileMut;
	}
}

