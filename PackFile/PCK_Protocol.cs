﻿using System;

namespace PackFile
{
	/**
		 * Packed File Format 
		 *
		 *--File Type Text, 8 Bytes
		 *--Version Text, 8 Bytes
		 *--Head Info Length, 8 Bytes
		 *--Files Info List Block
		 *----File Info 01
		 *------Path String Length, 8 Bytes
		 *------Path String, N Bytes
		 *------File Data Offset, 8 Bytes
		 *------File Data Size, 8 Bytes
		 *----File Info 02
		 *...
		 *----File Info 03
		 *...
		 *--Files Data Block
		 *----File Data 01, X1 Bytes
		 *----File Data 02, X2 Bytes
		 *----File Data 03, X3 Bytes
		 */

	class PCK_Protocol
	{
		public static string postfix = "pck";
		public static int FILE_TYPE_SIGN_SIZE = 8;
		public static int FILE_VERSION_SIZE = 8;
		public static int FILE_FILELIST_HEAD_LENGTH_SIZE = 8;

		public static int HEAD_PATH_STRING_LENGTH_SIZE = 8;
		//public static int HEAD_PATH_STRING_SIZE = N;
		public static int HEAD_FILE_OFFSET_SIZE = 8;
		public static int HEAD_FILE_SIZE_SIZE = 8;
		public static int FILE_HEAD_TOTAL_SIZE = FILE_TYPE_SIGN_SIZE + FILE_VERSION_SIZE + FILE_FILELIST_HEAD_LENGTH_SIZE;

		public static string FileTypeSign = "FLHS_PCK";
		public static string Version = "20151014";
	}
}

