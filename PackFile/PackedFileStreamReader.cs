﻿using System;
using System.IO;
using System.Threading;

namespace PackFile
{
	public class PackedFileStreamReader
	{
		public long Position = 0;
		public long Length;

		Stream stream;
		Mutex mut;
		long offset;

		public PackedFileStreamReader (Stream stream, Mutex mut, long offset, long size)
		{
			this.stream = stream;
			this.mut = mut;
			this.offset = offset;
			Length = size;
		}

		public int Read(byte[] buffer,int index,int count)
		{
			/*
			 * 	ArgumentException	
				缓冲区长度减去 index 小于 count。
				
				ArgumentNullException	
				buffer 为 null。
				
				ArgumentOutOfRangeException	
				index 或 count 为负。
				
				IOException	
				出现 I/O 错误，如流被关闭。
			*/
			try
			{
				if(buffer == null)
				{
					throw(new ArgumentException ("buffer is null."));
				}

				if (buffer.Length - index < count) 
				{
					throw(new ArgumentException ("buffer size minus index is less then count"));
				}

				if (index < 0 || count < 0) 
				{
					throw(new ArgumentOutOfRangeException ("index or count is negative"));
				}

				if(count > Length - Position)
				{
					count = (int)(Length - Position);
				}


				mut.WaitOne ();

				stream.Position = offset + Position;
				int length = stream.Read(buffer, index, count);

				mut.ReleaseMutex();

				Position += length;

				return length;
			}
			catch(Exception e) 
			{
				throw(e);
			}

		}
	}
}

